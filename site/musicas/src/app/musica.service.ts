import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';
declare var __dirname;

@Injectable({
  providedIn: 'root'
})
export class MusicaService {
  public _estrofeNum: number;
  public _musicaAtual: string;


  private _listaArquivos: string[];
  private _listaMusicas: any[] = []; // ['O pão da vida.mus', 'Só por ti Jesus.mus', 'A Barca.mus'];
  private _idMusicaAtual = 0;
  private _estrofeAtual: string[] = [];
  private _caminhoAssets = __dirname + '\\assets\\data\\';
  private _caminhoJsonFiles = __dirname + '\\assets\\data\\ListaArquivos.json';

  public set listaArquivos(value: string[]) {
    this._listaArquivos = value;
    console.log('DIR NAME ', __dirname)
    console.log(__dirname);

  }

  public get idMusicaAtual(): number {
    return this._idMusicaAtual;
  }

  public get listaMusicas(): any[] {
    return this._listaMusicas;
  }

  public get listaArquivos(): string[] {
    return this._listaArquivos;
  }

  public set listaMusicas(value: any[]) {
    this._listaMusicas = value;
  }

  public get estrofeAtual(): string[] {
    return this._estrofeAtual;
  }

  public set estrofeAtual(value: string[]) {
    this._estrofeAtual = value;
  }

  public get musicaAtual(): string {
    return this._musicaAtual.replace('.mus', '');
  }

  public set musicaAtual(value: string) {
    this._musicaAtual = value;
  }

  public get estrofeNum(): number {
    return this._estrofeNum;
  }

  public set estrofeNum(value: number) {
    this._estrofeNum = value;
  }

  public cargaMusica = new Subject<number>();
  constructor(public httpClient: HttpClient) {
    this._listaArquivos = [];
    this.cargaMusica
      .subscribe((num: number) => {
        this._estrofeNum += num;
        this.getStrofe();
      });
  }

  getStrofe() {
    this._estrofeAtual = [];
    console.log('get strofe', this._caminhoAssets + this._musicaAtual + '.mus');
    console.log(this._caminhoAssets + this._musicaAtual + '.mus');
    console.log(this._musicaAtual + '.mus');
    console.log('DIR NAME ', __dirname)
    console.log('DIRNAME ' + __dirname);
    return this.httpClient.get(this._caminhoAssets + this._musicaAtual+ '.mus', { responseType: 'text' })
      .subscribe(musica => {
        const letras = musica.split(String.fromCharCode(10));
        const a = [];
        let estrofe = 0;
        Promise.all(letras.map(async (element: string) => {
          console.log(this.estrofeNum, estrofe);
          console.log('element  ', element);
          if (this.estrofeNum === estrofe) {
            a.push(element);
          }
          if (element.trim() === '') {
            estrofe++;
          }
        }));
        if (a.length === 0) {
          this._idMusicaAtual++;
          this._estrofeNum = 0;
          this._musicaAtual = this._listaMusicas[this._idMusicaAtual].arquivo;
        }
        this._estrofeAtual = a;
      });
  }


  getListaArquivos() {
    this._estrofeAtual = [];
    console.log('chamando html', this._caminhoJsonFiles);
    console.log(__dirname);
    console.log(this._caminhoJsonFiles);
    return this.httpClient.get(this._caminhoJsonFiles, { responseType: 'text' })
      .pipe(map(aa => {
        this._listaArquivos = JSON.parse(aa);
      }));
  }

  next() {
    this._estrofeNum++;
    this.getStrofe();

  }

  previous() {
    if (this._estrofeNum > 0) {
      this._estrofeNum--;
    }
    this.getStrofe();
  }

  begin() {
    this._estrofeNum = 1;
    this.getStrofe();
  }
}
