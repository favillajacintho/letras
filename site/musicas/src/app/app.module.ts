
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToolbarModule } from 'primeng/toolbar';
import { ButtonModule } from 'primeng/button';
import { SidebarModule } from 'primeng/sidebar';
import { ListboxModule } from 'primeng/listbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownModule } from 'primeng/dropdown';
import { TableModule } from 'primeng/table';
import { PickListModule } from 'primeng/picklist';


import { AppComponent } from './app.component';
import { MusicaComponent } from './musica/musica.component';
import { MusicaService } from './musica.service';
import { RodapeComponent } from './rodape/rodape.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    MusicaComponent,
    RodapeComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ToolbarModule,
    ButtonModule,
    SidebarModule,
    ListboxModule,
    DropdownModule,
    PickListModule,
    TableModule
  ],
  providers: [MusicaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
