import { MusicaService } from './../musica.service';
import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-musica',
  templateUrl: './musica.component.html',
  styleUrls: ['./musica.component.css']
})
export class MusicaComponent implements OnInit {

  musicaAtual = 'A Barca.mus';
  estrofe = 2;
  estrofeAtual = [];
  listaMusicas = [];
  constructor(public musicaSrv: MusicaService) {
    this.musicaSrv._estrofeNum = 2;
    this.musicaSrv._musicaAtual = 'A Barca.mus';

    this.musicaSrv.cargaMusica.next(1)
  }

  ngOnInit() {
  }


}
