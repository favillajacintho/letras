import { Component, OnInit } from '@angular/core';
import { MusicaService } from './../musica.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  display = false;
  lista = [];
  cols: any[];
  selectedMusic: any[] = [];
  constructor(public musicaSrv: MusicaService) {
    this.cols = [
      { field: 'arquivo', header: 'Música' }
    ];
  }

  ngOnInit() {
  }

  next() {
    this.musicaSrv.cargaMusica.next(1);
  }

  previous() {
    this.musicaSrv.cargaMusica.next(-1);
  }

  sidebar() {
    this.lista = [];
    this.musicaSrv.getListaArquivos()
      .subscribe(data => {
        this.musicaSrv.listaArquivos.forEach((elemento: any) => {
          this.lista.push({ label: elemento.titulo, value: elemento.arquivo });
        });
        this.display = !this.display;
      });
  }

  AtualizarMusica(event) {
    this.musicaSrv._musicaAtual = event.value;
    this.musicaSrv._estrofeNum = 1;
    this.musicaSrv.getStrofe();
    this.display = false;

  }

  fechar() {
    this.musicaSrv.listaMusicas = this.selectedMusic;
    this.musicaSrv.estrofeNum = 0;
    this.musicaSrv.musicaAtual = this.musicaSrv.listaMusicas[this.musicaSrv.idMusicaAtual].arquivo;
    this.display = false;
  }
}
