// import { MusicaService } from './../../service/musica/musica.service';
import { Controller, Get, Param, Response, Query, Res } from '@nestjs/common';
import { HttpService } from '@nestjs/common';
import * as fs from 'fs-plus';
import * as logger4js from 'log4js';
import * as recursive from 'recursive-readdir';
import * as replace from 'replaceall';

import { readdirSync, statSync } from 'fs';
import { join } from 'path';
import { resolve } from 'dns';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Controller('musica')
export class MusicaController {

  private _letras: any = {};
  private _filtro = '.mus';
  private log = logger4js.getLogger();
  private tempFile = [];

  public get letras(): any {
    return this._letras;
  }

  private _listaMusicas = [];
  private _path = './src/assets/data/';

  constructor(private readonly http: HttpService) {
    this.MontarListaArquivos(this._filtro);
    // .then(data => {
    console.log('voltando do montar lista', this._listaMusicas);
    // });
  }

  @Get()
  async FileList(@Response() res, next) {
    const localList = [];
    res.send(200, this._listaMusicas);
    return this._listaMusicas;
  }

  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  MontarListaArquivos(filtro) {
    const that = this;
    fs.readFile(that._path + '/ListaArquivos.JSON', 'utf8', async function s(err, data) {
      try {
        //   console.log(data);
        that._listaMusicas = data;
        //  that._letras.Estrofes = data.split(String.fromCharCode(10));
      } catch (error) {
        that._listaMusicas = [];
      }
    });
  }

  getFilesList(items: any) {
    const that = this;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < items.length; i++) {
      const filename = items[i];
      if (filename.indexOf(that._filtro) !== -1) {
        fs.readFile(that._path + filename, 'utf8', function s(err, data) {
          if (err) {
            console.log(err); throw err;
          }
          that._listaMusicas.push({ arquivo: filename, titulo: data.split('\n')[0] });
        });
      }
    }
  }

  getContentJSON(@Param() params) {
    this.http.get(params.arquivo, { responseType: 'text' })
      .subscribe(res => {
        this._letras.Estrofes = res.data.split(String.fromCharCode(10));
      });
  }

  @Get('strofes')
  async getEstrofe(@Query() params, @Response() res) {
    const that = this;
    const arquivo = params.arquivo;
    const num = params.num;
    const estrofe = 0;
    let a = [];
    await this.lerArquivo(arquivo)
      .subscribe((data: any) => {
        console.log('arquivo ', arquivo)
        console.log(data)
        this.lerStrofe(data, num)
          .subscribe(xpto => {
            a = xpto;
            res.send(200, a);
          });
      });
    return a;
  }

  lerArquivo(arquivo): Observable<any> {
    return Observable.create((observer) => {
      const that = this;
      fs.readFile(that._path + arquivo, 'utf8', async function s(err, data) {
        try {
          that._letras.Estrofes = data.split(String.fromCharCode(10));
        } catch (error) {
          that._letras.Estrofes = [];
        }

        observer.next(that._letras.Estrofes);
        observer.complete();
      });
    });
  }

  lerStrofe(letras, num): Observable<any> {
    return Observable.create((observer) => {
      let estrofe = 0;
      const a = [];
      console.log('num ', num);
      Promise.all(letras.map(async (element: string) => {
        console.log(num, estrofe);
        console.log('element  ', element);
        if (num == estrofe) {
          a.push({
            frase: element,
            ordem: 1,
            refrao: false,
          });
        }
        if (element.trim() === '') {
          estrofe++;
        }

      }));
      console.log('a ao final ', a);
      observer.next(a);
      observer.complete();
    });
  };

  @Get('montar')
  cargarHTMLtoText() {
    const pastas = [
      'D:/Downloaded Web Sites2/www.letras.mus.br/catolicas',
      'C:/Downloaded Web Sites/Eliana/www.letras.mus.br/eliana-ribeiro',
      'D:/Downloaded Web Sites/anjos/www.letras.mus.br/anjos-de-resgate-musicas',
      'D:/Downloaded Web Sites/arautos/www.letras.mus.br/arautos-do-rei',
      'D:/Downloaded Web Sites/celina-borges/www.letras.mus.br/celina-borges',
      'D:/Downloaded Web Sites/Padre Marcelo/www.letras.mus.br/padre-marcelo-rossi',
      'D:/Downloaded Web Sites/pe-fabio-de-melo/www.letras.mus.br/pe-fabio-de-melo',
      'D:/Downloaded Web Sites/Shalon/www.letras.mus.br/banda-shalom',
      'D:/Downloaded Web Sites/www.letras.mus.br/aline-barros',
      'D:/Downloaded Web Sites/padre-reginaldo-manzotti/www.letras.mus.br/padre-reginaldo-manzotti'
    ];

    const jsonFile = [];
    pastas.forEach(pasta => {
      recursive(pasta, [], function x(err, files) {
        // const lista = [];
        const i = 1;
        const cont = 1;
        // console.log('-==================================-', files)
        files.forEach(element => {
          //    console.log(lista)
          if (element.indexOf('index.html') !== -1) {
            // lista.push(element);
            fs.readFile(element, 'utf8', async function s(err, data) {
              //  const conteudo = string[];
              const tituloinicio = data.indexOf('<div class="cnt-head_title') + 25;
              const temp = data.substr(tituloinicio, 150);
              let i = temp.indexOf('<h1>') + 4;
              let f = temp.indexOf('</h1>');
              let titulo = temp.substr(i, (f - i));
              let conteudo = titulo + '\n \n';
              titulo = replace('?', '', titulo);

              i = data.indexOf('<article>') + 13;
              f = data.indexOf('</article>');

              conteudo = conteudo += data.substr(i, (f - i));
              conteudo = replace('</p>', ' \n', conteudo);
              conteudo = replace('<p>', ' \n', conteudo);
              conteudo = replace('<br/>', ' \n', conteudo);
              conteudo = replace('<br>', ' \n', conteudo);

              // console.log(titulo)
              // tslint:disable-next-line:max-line-length
              fs.writeFile('D:\\projetos\\paroquia\\Gestao_Letras\\letras-backend\\src\\assets\\data\\' + titulo + '.mus', conteudo, (err) => {
                // throws an error, you could also catch it here
                if (err) throw err;
                jsonFile.push({ arquivo: titulo, nome: titulo })
                // console.log('Lyric saved!');
              });

              fs.writeFile('D:\\projetos\\paroquia\\Gestao_Letras\\letras-backend\\src\\assets\\data\\ListaArquivos.json',
                JSON.stringify(jsonFile), (err) => {
                  // throws an error, you could also catch it here
                  if (err) throw err;
                });
            });
          }
        });
      });
    });
  }

}
