import { Test, TestingModule } from '@nestjs/testing';
import { MusicaController } from './musica.controller';

describe('Musica Controller', () => {
  let module: TestingModule;
  
  beforeAll(async () => {
    module = await Test.createTestingModule({
      controllers: [MusicaController],
    }).compile();
  });
  it('should be defined', () => {
    const controller: MusicaController = module.get<MusicaController>(MusicaController);
    expect(controller).toBeDefined();
  });
});
