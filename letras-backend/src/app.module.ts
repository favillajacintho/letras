import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MusicaController } from './controller/musica/musica.controller';
// import { MusicaService } from './service/musica/musica.service';

@Module({
  imports: [HttpModule],
  controllers: [AppController, MusicaController],
  providers: [AppService],
})
export class AppModule { }
